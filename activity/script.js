// Activity:
// 1. In the S19 folder, create an activity folder and an index.html and index.js file inside of it.
// 2. Link the script.js file to the index.html file.

// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

const cube = "2";
const getCube =  cube * cube * cube;

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

console.log(`The cube of ${cube} is ${getCube}`) 

// 5. Create a variable address with a value of an array containing details of an address.
const address = ["258 Washington Ave NW", " California", " 90011"]
const [streetAddress, state, zipCode] = address
console.log(zipCode)
console.log(streetAddress)
console.log(state)
// 6. Destructure the array and print out a message with the full address using Template Literals.
console.log(`I live at ${address}`)
// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
const animal = {
	kind: "crocodile",
	type: "saltwater", 
	name: "Lolong",
	weight: 1075, 
	height: "20 ft 3 in"
}
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
const { kind, type, name, weight, height} = animal
console.log(`${name} was a ${type} ${kind}. He weighed at ${weight}kgs with a measurement of ${height}.`)
// 9. Create an array of numbers.
const numbers = [5,4,3,2,1]
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
numbers.forEach((num) => console.log(`${num}`));
console.log(`HAPPY NEW YEAR!!!`)
// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
const reduceNumber = (previousValue, currentValue) => previousValue + currentValue;
console.log(numbers.reduce(reduceNumber));
// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
class dog {
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}
// 13. Create/instantiate a new object from the class Dog and console log the object.
const myDog = new dog();
myDog.name = 'Winter';
myDog.age = '3';
myDog.breed = 'Filipino Dog';
console.log(myDog)
// 14. Create a git repository named S19.
// 15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code. "s19 Activity"
// 16. Add the link in Boodle. "Javascript ES6 Updates